'use strict';
async function addUserFetch(userInfo) {
  if (typeof userInfo !== 'object')
    throw new Error('UserInfo должно быть объектом');
  if (!('id' in userInfo)) {
    const res = await fetch('/users', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify(userInfo)
    });
    if (res.status >= 400) {
      throw new Error(`ServerError: ${res.status} ${res.statusText}`);
    }
    const json = await res.json();
    return json.id;
  } else {
    throw new Error('UserInfo не должен содержать поле "id"');
  }
  // напишите POST-запрос используя метод fetch
}

async function getUserFetch(id) {
  if (typeof id !== 'number') {
    throw new Error('Параметр "id" должен быть числом');
  }
  const res = await fetch(`/users/${id}`);
  if (res.status >= 400) {
    throw new Error(`ServerError: ${res.status} ${res.statusText}`);
  }
  return await res.json();
  // напишите GET-запрос используя метод fetch
}

async function addUserXHR(userInfo) {
  if (typeof userInfo !== 'object')
    throw new Error('UserInfo должно быть объектом');
  if (!('id' in userInfo)) {
    const promise = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('POST', '/users');
      xhr.responseType = 'json';
      xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
      xhr.send(JSON.stringify(userInfo));
      xhr.onload = () => {
        if (+xhr.status.toString()[0] & 0xfd) {
          reject(new Error(`ServerError: ${xhr.status} ${xhr.statusText}`));
        }
        resolve(xhr.response.id);
      };
      xhr.onerror = () => {
        reject(new Error(`ServerError: ${xhr.status} ${xhr.statusText}`));
      };
    });
    return promise;
  } else {
    throw new Error('UserInfo не должен содержать поле "id"');
  }
  // напишите POST-запрос используя XMLHttpRequest
}

async function getUserXHR(id) {
  if (typeof id !== 'number') {
    throw new Error('Параметр "id" должен быть числом');
  }
  const promise = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `/users/${id}`);
    xhr.responseType = 'json';
    xhr.send(id);
    xhr.onload = () => {
      if (+xhr.status >= 400) {
        reject(new Error(`ServerError: ${xhr.status} ${xhr.statusText}`));
      }
      resolve(xhr.response);
    };
    xhr.onerror = () => {
      reject(new Error(`ServerError: ${xhr.status} ${xhr.statusText}`));
    };
  });
  return promise;
}

function checkWork() {
  addUserFetch({ name: "Alice", lastname: "FetchAPI" })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserFetch(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });

  addUserXHR({ name: "Ecila", lastname: "XHR" })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

checkWork();